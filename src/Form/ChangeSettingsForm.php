<?php

namespace Drupal\change\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Routing\RouteBuilderInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\Core\StringTranslation\TranslationInterface;
use Drupal\change\Config;
use Drupal\change\EventSubscriber\RenameAdminPathsEventSubscriber;
use Symfony\Component\DependencyInjection\ContainerInterface;

class ChangeSettingsForm extends ConfigFormBase {

  use StringTranslationTrait;

  /**
   * @var Config
   */
  private $config;

  /**
   * @var RouteBuilderInterface
   */
  private $routeBuilder;

  /**
   * {@inheritdoc}.
   */
  public function getFormId(): string {
    return 'change_settings_form';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames(): array {
    return [
      Config::CONFIG_KEY,
    ];
  }

  /**
   * @param Config $config
   * @param RouteBuilderInterface $routeBuilder
   * @param TranslationInterface $stringTranslation
   */
  public function __construct(
    Config $config,
    RouteBuilderInterface $routeBuilder,
    TranslationInterface $stringTranslation
  ) {
    $this->config = $config;
    $this->routeBuilder = $routeBuilder;
    $this->stringTranslation = $stringTranslation;
  }

  /**
   * @param ContainerInterface $container
   *
   * @return static
   */
  public static function create(ContainerInterface $container): self {
    return new static(
      $container->get('Drupal\change\Config'),
      $container->get('router.builder'),
      $container->get('string_translation')
    );
  }

  /**
   * Returns this modules configuration object.
   */
  /*protected function getConfig() {
    return $this->config($this->config);
  }*/

  /**
   * {@inheritdoc}.
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form['admin_path'] = [
      '#type' => 'details',
      '#title' => $this->t('Admin path'),
      '#open' => TRUE,
    ];

    //$config = $this->getConfig();

    $form['admin_path']['admin_path'] = [
      '#type'          => 'checkbox',
      '#title'         => $this->t('Change admin path'),
      '#default_value' => $this->config->isPathEnabled('admin'),
      '#description'   => $this->t('If checked, "admin" will be replaced by the following term in admin path.'),
    ];
    $form['admin_path']['admin_path_value'] = [
      '#type'             => 'textfield',
      '#title'            => $this->t('Default admin path'),
      '#default_value'    => $this->config->getPathValue('admin'),
      '#description'      => $this->t('Specify a machine name to replace as the admin path.'),
      '#element_validate' => [[$this, 'validate']],
    ];


    $form['user_path'] = [
      '#type' => 'details',
      '#title' => $this->t('Change user path'),
      '#open' => TRUE,
    ];
    $form['user_path']['user_path'] = [
      '#type'          => 'checkbox',
      '#title'         => $this->t('Rename user path'),
      '#default_value' => $this->config->isPathEnabled('user'),
      '#description'   => $this->t('If checked, "user" will be replaced by the following term in user path.'),
    ];
    $form['user_path']['user_path_value'] = [
      '#type'             => 'textfield',
      '#title'            => $this->t('Replace "user" in user path by'),
      '#default_value'    => $this->config->getPathValue('user'),
      '#description'      => $this->t('Specify a machine name to replace as the user path.'),
      '#element_validate' => [[$this, 'validate']],
    ];

    $routes = [
      'login' => $this->t('Login'),
      'register' => $this->t('Registration'),
      'password' => $this->t('Reset password'),
      'logout' => $this->t('Logout'),
    ];

    foreach ($routes as $key => $label) {
      $form[$key . '_settings'] = [
        '#type' => 'fieldset',
        '#title' => $this->t('@label route settings', ['@label' => $label,]),
      ];
      $form[$key . '_settings'][$key . '_disabled'] = [
        '#type' => 'checkbox',
        '#title' => $this->t('Disable default user/@key route', ['@key' => $key,]),
        '#description' => $this->t('This setting will force the user/@key route to be disabled or updated.', ['@key' => $key,]),
        //'#default_value' => $config->get($key . '_disabled'),
      ];
      $form[$key . '_settings'][$key . '_route'] = [
        '#type' => 'textfield',
        '#title' => $this->t('Replace user/@key with:', ['@key' => $key,]),
        '#description' => $this->t('Leave empty to make the route completely disabled.'),
        //'#default_value' => $config->get($key . '_route'),
        '#required' => FALSE,
        '#states' => [
          'disabled' => [
            ':input[name="' . $key . '_disabled"]' => ['checked' => FALSE],
          ],
        ],
      ];
      $form[$key . '_settings'][$key . '_noindex'] = [
        '#type' => 'checkbox',
        '#title' => $this->t('Set user/@key route to noindex', ['@key' => $key,]),
        '#description' => $this->t('This setting will add a header on the user/@key route to be noindex.', ['@key' => $key,]),
        //'#default_value' => $config->get($key . '_noindex'),
      ];
    }

    return parent::buildForm($form, $form_state);
  }

  /**
   * Form element validation handler.
   *
   * @param array $element
   * @param FormStateInterface $formState
   */
  public function validate(&$element, FormStateInterface $formState) {
    if (empty($element['#value'])) {
      $formState->setError(
        $element,
        $this->t('Path replacement value must contain a value.')
      );
    }
    elseif (!RenameAdminPathsValidator::isValidPath($element['#value'])) {
      $formState->setError(
        $element,
        $this->t(
          'Path replacement value must contain only letters, numbers, hyphens and underscores.'
        )
      );
    }
    elseif (RenameAdminPathsValidator::isDefaultPath($element['#value'])) {
      $formState->setError(
        $element,
        sprintf(
          $this->t('Renaming to a default name (%s) is not allowed.'),
          implode(', ', RenameAdminPathsEventSubscriber::ADMIN_PATHS)
        )
      );
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $formState) {
    $this->saveConfiguration($formState);

    // at this stage we rebuild all routes to use the new renamed paths
    $this->routeBuilder->rebuild();

    // add confirmation message
    parent::submitForm($form, $formState);

    // make sure we end up at the same form again using the new path
    $formState->setRedirect('change.admin');
  }

  /**
   * @param FormStateInterface $formState
   */
  private function saveConfiguration(FormStateInterface $formState) {
    $this->config->setPathEnabled('admin', $formState->getValue('admin_path'));
    $this->config->setPathValue(
      'admin',
      $formState->getValue('admin_path_value')
    );
    $this->config->setPathEnabled('user', $formState->getValue('user_path'));
    $this->config->setPathValue(
      'user',
      $formState->getValue('user_path_value')
    );
    $this->config->save();
  }
}
